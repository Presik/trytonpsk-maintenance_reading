Module Maintenance Reading
##########################

Automatic meter reading, or AMR, is the technology of automatically collecting consumption, 
diagnostic, and status data from water meter or energy metering devices (gas, electric) and 
transferring that data to a central database for billing, troubleshooting, and analyzing.

This technology mainly saves utility providers the expense of periodic trips to each 
physical location to read a meter. Another advantage is that billing can be based on
near real-time consumption rather than on estimates based on past or predicted consumption.
This timely information coupled with analysis can help both utility providers
and customers better control the use and production of electric energy, 
gas usage, or water consumption.


[1] www.wikipedia.org
