# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.model import fields
from trytond.pool import PoolMeta
from trytond.pyson import Eval


class ShipmentInternal(metaclass=PoolMeta):
    __name__ = 'stock.shipment.internal'
    vehicle = fields.Many2One('maintenance.equipment', 'Vehicle', domain=[
            ('type_device', '=', 'vehicle'),
        ], states={
            'readonly': Eval('state') != 'draft',
            'required': Eval('state') != 'draft',
        })
