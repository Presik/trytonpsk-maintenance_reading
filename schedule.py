# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.pool import Pool, PoolMeta


class Schedule(metaclass=PoolMeta):
    __name__ = 'maintenance.schedule'

    @classmethod
    def __setup__(cls):
        super(Schedule, cls).__setup__()

    @classmethod
    def analize_readings(cls, fline, line):
        Input = Pool().get('maintenance.input_reading')
        delta_values = Input.search([
            ('equipment', '=', line.planning.equipment.id),
            ('meter_reading', '=', fline.frecuency_kind.meter_reading.id),
        ], order=[('timestamp_effective', 'ASC')])
        if delta_values:
            if fline.frecuency_kind.meter_reading.type_input == 'incremental':
                start_delta_value = delta_values[0].value
                end_delta_value = delta_values[-1].value
                delta_value = end_delta_value - start_delta_value
            else:
                delta_value = sum([d.value for d in delta_values])
            if delta_value >= fline.value:
                return True
        return False
