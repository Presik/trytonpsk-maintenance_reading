# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from datetime import datetime, date
from decimal import Decimal
from trytond.model import Workflow, ModelView, ModelSQL, fields, Unique
from trytond.wizard import Wizard, StateView, Button, StateReport, StateTransition
from trytond.pyson import Eval
from trytond.pool import PoolMeta, Pool
import json
import requests
from sql import Table

STATES = {
    'readonly': (Eval('state') != 'draft'),
}


_ZERO = Decimal('0.0')


class InputReading(Workflow, ModelSQL, ModelView):
    'Input Reading'
    __name__ = 'maintenance.input_reading'
    meter_reading = fields.Many2One('maintenance.meter_reading',
            'Meter Reading', required=True, states=STATES)
    equipment = fields.Many2One('maintenance.equipment', 'Equipment',
            required=True, states=STATES)
    timestamp_effective = fields.DateTime('Timestamp Effective', required=True,
            states=STATES)
    value = fields.Numeric('Value', digits=(16, 2), states=STATES,
            required=True)
    state = fields.Selection([
            ('draft', 'Draft'),
            ('cancel', 'Cancel'),
            ('done', 'Done'),
    ], 'State', readonly=True, required=True)

    @classmethod
    def __setup__(cls):
        super(InputReading, cls).__setup__()
        table = cls.__table__()
        cls._transitions |= set((
                ('draft', 'done'),
                ('done', 'draft'),
                ('draft', 'cancel'),
        ))
        cls._buttons.update({
                'draft': {
                    'invisible': Eval('state') != 'draft',
                    },
                'cancel': {
                    'invisible': Eval('state') != 'draft',
                    },
                'done': {
                    'invisible': Eval('state') != 'draft',
                    },
                })
        cls._sql_constraints += [
           ('equipment_timestamp_effective_meter_reading_uniq', Unique(table, table.timestamp_effective, table.equipment, table.meter_reading),
               'Equipment already exists for date!'),
        ]

    @staticmethod
    def default_state():
        return 'draft'

    @staticmethod
    def default_timestamp_effective():
        return datetime.now()

    @classmethod
    @ModelView.button
    @Workflow.transition('draft')
    def draft(cls, inputs):
        pass

    @classmethod
    @ModelView.button
    @Workflow.transition('cancel')
    def cancel(cls, inputs):
        pass

    @classmethod
    @ModelView.button
    @Workflow.transition('done')
    def done(cls, records):
        for record in records:
            pass

    @classmethod
    def create_reading_report(cls, inputx):
        '''
        Create Reading Meter Report

        This method to create a register of value delta between last
        input and new input reading
        '''
        ReadingReport = Pool().get('maintenance.reading_report')
        prev_input = cls.search([
                    ('code', '=', inputx.code)],
                    limit=2,
                    order=[('id', 'DESC')])
        uom = inputx.code.uom_timedelta
        if len(prev_input) < 2:
            start_period = None
            timedelta = None
            delta = None
            total_cost = _ZERO
        else:
            prev_input = prev_input[1]
            start_period = prev_input.timestamp_effective
            timedelta = inputx.compute_uom_timedelta(
                    prev_input.timestamp_effective,
                    inputx.timestamp_effective, uom)
            delta = inputx.input_reading - prev_input.input_reading
            total_cost = Decimal(delta * inputx.code.cost_value)
        reading = ReadingReport.create([
                {
                'equipment': inputx.equipment,
                'start_period': start_period,
                'end_period': inputx.timestamp_effective,
                'uom_timedelta': inputx.code.uom_timedelta,
                'timedelta': timedelta,
                'uom_meter_reading': inputx.uom_meter_reading,
                'reading_delta': delta,
                'total_cost': total_cost,
        }])
        return reading

    def compute_uom_timedelta(self, start_date, end_date, uom):
        #FIXME !!!!!!!!!!!!
        if not start_date or not end_date:
            return None
        dtime = end_date - start_date
        dtime = dtime.total_seconds()

        if uom.id == 11: #id for Day uom
            timedelta = (dtime / 3600)/24
        elif uom.name == 9: #id for Hour uom
            timedelta = dtime / 3600
        elif uom.name == 7: #id for Second uom
            timedelta =  dtime
        else:
            timedelta = 0
            # print "Warning ... uom doesn't recognized"
        timedelta = Decimal(str(round(timedelta, 2)))
        return timedelta

    @classmethod
    def import_data(cls, fields_names, data):
        #FIXME: Add selection of interface to import
        if not fields_names:
            fields_names = ('equipment', 'meter_reading',
                    'timestamp_effective', 'value', 'state')
            data = cls._convert_satrack(data)
        return super(InputReading, cls).import_data(fields_names, data)

    @classmethod
    def _convert_satrack(cls, data):
        reading_to_create = []
        if not data:
            return reading_to_create
        for row in data[1:]:
            date_ = datetime(int(row[1]), int(row[2]), int(row[3]), 23, 0, 0)
            for mr_code, value in (('DR', row[4]), ('HU', row[5])):
                value = value.replace(',', '.')
                reading_to_create.append([
                    row[0], mr_code, str(date_), value, 'done'
                ])
        return reading_to_create

    @classmethod
    def load_gps_data(cls, url, data):
        response = requests.post(url, data)
        if response.status_code == 200:
            return json.loads(response.text)


class ReadingReport(ModelSQL, ModelView):
    'Reading Report'
    __name__ = 'maintenance.reading_report'
    equipment = fields.Many2One('maintenance.equipment', 'Equipment',
            readonly=True)
    uom_meter_reading = fields.Many2One('product.uom', 'UoM Meter Reading',
            readonly=True)
    start_period = fields.DateTime('Start Period', readonly=True)
    end_period = fields.DateTime('End Period', readonly=True)
    uom_timedelta = fields.Many2One('product.uom', 'UoM Timedelta',
            readonly=True)
    timedelta = fields.Numeric('Timedelta', digits=(16, 2), readonly=True)
    reading_delta = fields.Numeric('Reading Delta', digits=(16, 2), readonly=True)
    total_cost = fields.Numeric('Total Cost',  digits=(16, 2), readonly=True)

    @classmethod
    def __setup__(cls):
        super(ReadingReport, cls).__setup__()


class LoadGpsDataStart(ModelView):
    'Load Gps Data Start'
    __name__ = 'maintenance.input_reading.load_gps_data.start'
    start_date = fields.Date('Start Date', required=True)
    meter_reading = fields.Many2One('maintenance.meter_reading', 'Meter Reading',
            required=True)

    @staticmethod
    def default_start_date():
        Date = Pool().get('ir.date')
        return Date.today()


class LoadGpsData(Wizard):
    'Load GPS Data'
    __name__ = 'maintenance.input_reading.load_gps_data'
    """
    this is the wizard that allows create input reading taking data in api.
    """
    start = StateView('maintenance.input_reading.load_gps_data.start',
        'maintenance_reading.load_gps_data_view_form', [
            Button('Cancel', 'end', 'tryton-cancel'),
            Button('Create', 'accept', 'tryton-ok', default=True),
        ])
    accept = StateTransition()

    def transition_accept(self):
        pool = Pool()
        InputReading = pool.get('maintenance.input_reading')
        Configuration = pool.get('maintenance.configuration')
        Equipment = pool.get('maintenance.equipment')
        Company = pool.get('company.company')
        config = Configuration(1)
        start_date = self.start.start_date
        meter_reading = self.start.meter_reading
        res = {}
        params = {}
        value = 0

        if config.url_api and config.api_key:
            date_string = str(start_date.day)+'/'+str(start_date.month)+'/'+str(start_date.year)
            # Company.convert_timezone(start_date)
            params = {
                "FechaConsulta": date_string,
                "Token": config.api_key,
                }
            res = InputReading.load_gps_data(config.url_api, params)

        if res:
            message = res['Mensaje']
            data = res['Datos']

            for d in data:
                # equipments_dates = {'equipment_id': input.equipment.id, 'timestamp_effective': input.timestamp_effective for input in InputReading.search([])}
                equipments = Equipment.search([
                    ('product.name', '=', d['Placa']),
                ])
                if equipments:
                    date_response = d['Fecha']
                    date_response = date_response.replace(' AM', '')

                    if meter_reading.uom_reading.category.name == 'Longitud':
                        value = round(Decimal(d['DistanciaRecorrida']),2)
                    if meter_reading.uom_reading.category.name == 'Tiempo':
                        if meter_reading.uom_reading.name == 'Hora':
                            value = round((Decimal(d['MinutosMovimiento']) / 60), 2)
                        elif meter_reading.uom_reading.name == 'Segundo':
                            value = round((Decimal(d['MinutosMovimiento']) / 3600), 2)
                        else:
                            value = round(Decimal(d['MinutosMovimiento']), 2)
                    new_input = {
                        'equipment': equipments[0].id,
                        'timestamp_effective': Company.convert_timezone(datetime.strptime(date_response, '%m/%d/%Y %H:%M:%S'), True),
                        'value': value,
                        'state': 'done',
                        'meter_reading': meter_reading.id,
                    }
                    InputReading.create([new_input])

        return 'end'
